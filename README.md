# SLUD2019

SLUD 2019: XVI Semana Linux, evento organizado por el GLUD y desarrollado en la Universidad Distrital Francisco José de Caldas.

Llevada a cabo del 26 al 31 de Agosto de 2019.

Se incluyen diferentes formatos (impresión(CMYK),PDF, TIF, Digital(RGB), SVG, etc.)

Algunos archivos estan desarrollados con software privativo, debido a políticas en temas de impresión.

#### Contenido

+ Afiche Oficial
	+ Afiche oficial
	+ Afiche Hackathon
	+ Afiche Mapathon
+ Impresión
    + Afiche última versión aprobada
    + Escarapelas
    + Hackathon
    + Logo
    + Afiche redes sociales
+ Camiseta
+ Facebook
+ Logos
	+ XVI Semana Linux
	+ Universidad Distrital
	+ GLUD
+ Video


Desarrollo a cargo de: 

Código  | Nombre
------------- | -------------
20181020081  | Cristhian M. Yara P.


